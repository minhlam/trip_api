import unittest
from testing.mock_data import MockData
from main import create_app


class TestShowTripDetail(unittest.TestCase):
    def setUp(self):
        self.app = create_app(config='TEST')
        self.client = self.app.test_client()

    def test_show_trip_detail_1(self):
        data = MockData.mock_data_null()
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accepted': mimetype
        }
        result = self.client.post('/show_trip', data=data, headers=headers)
        self.assertEqual({"response": "NO DATA RECEIVE"}, result.json)

    def test_show_trip_detail_2(self):
        data = MockData.mock_data_show_trip_detail()
        data_result = MockData.mock_data_show_trip_detail_result()
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accepted': mimetype
        }
        result = self.client.post('/show_trip', data=data, headers=headers)
        self.assertEqual({'response': data_result}, result.json)

    def test_show_trip_detail_3(self):
        data = MockData.mock_data_show_trip_miss_attribute()
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accepted': mimetype
        }
        result = self.client.post('/show_trip', data=data, headers=headers)
        self.assertEqual({"response": "MISSING DATA"}, result.json)

    def test_show_trip_detail_4(self):
        data = MockData.mock_data_show_trip_negative_lat_lng()
        data_result = MockData.mock_data_show_trip_negative_lat_long_result()
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accepted': mimetype
        }
        result = self.client.post('/show_trip', data=data, headers=headers)
        self.assertEqual({'response': data_result}, result.json)


if __name__ == '__main__':
    unittest.main()
