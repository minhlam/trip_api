import unittest
from testing.mock_data import MockData
from main import create_app
from trip.trip_api import update_trip


class TestTripUpdate(unittest.TestCase):
    def setUp(self):
        self.app = create_app(config='TEST')

    def test_update_1(self):
        data = None
        result = update_trip("5dea6d7bd66e08f60ff1df6d", data)
        self.assertEqual({"response": "NO DATA"}, result)

    def test_update_2(self):
        data = MockData.mock_data_null()
        result = update_trip(None, data)
        self.assertEqual({"response": "MISSING SERVICE_TRIP_ID"}, result)

    def test_update_3(self):
        data = MockData.mock_data_normal()
        result = update_trip(None, data)
        self.assertEqual({"response": "MISSING SERVICE_TRIP_ID"}, result)

    def test_update_4(self):
        data = MockData.mock_data_update()
        result = update_trip("5dea6d7bd66e08f60ff1df6d", data)
        self.assertEqual({"response": "SUCCESS"}, result)

    def test_update_5(self):
        data = MockData.mock_data_null()
        result = update_trip(-1, data)
        self.assertEqual("Error", result)

    def test_update_6(self):
        data = MockData.mock_data_update()
        result = update_trip(-1, data)
        self.assertEqual("Error", result)


if __name__ == '__main__':
    unittest.main()
