import unittest
from testing.mock_data import MockData
from main import create_app


class TestPassengerRejected(unittest.TestCase):
    def setUp(self):
        self.app = create_app(config='TEST')
        self.client = self.app.test_client()

    def test_post_1(self):
        data = MockData.mock_data_null()
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/pass_rejected', data=data, headers=headers)
        self.assertEqual({"response": "NO DATA RECEIVE"}, result.json)

    def test_post_2(self):
        data = MockData.mock_data_reject()
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/pass_rejected', data=data, headers=headers)
        self.assertEqual({"response": "PASSENGER CANCELED"}, result.json)

    def test_post_3(self):
        data = MockData.mock_data_miss_attribute()
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/pass_rejected', data=data, headers=headers)
        self.assertEqual({"response": "MISSING PASSENGER RESPONSE"}, result.json)

    def test_post_4(self):
        data = MockData.mock_data_not_exist()
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/pass_rejected', data=data, headers=headers)
        self.assertEqual({"response": "NO TRIP ID IS 0"}, result.json)


if __name__ == '__main__':
    unittest.main()
