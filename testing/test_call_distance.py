import unittest
from main import create_app
from trip.trip_api import cal_distance


class TestCallDistance(unittest.TestCase):
    def setUp(self):
        self.app = create_app(config='TEST')

    def test_call_distance_1(self):
        result = cal_distance(latitude_from=None, longitude_from=None, latitude_to=None, longitude_to=None)
        self.assertEqual(-1, result)

    def test_call_distance_2(self):
        result = cal_distance(latitude_from=10.77667, longitude_from=106.66648,
                              latitude_to=10.75397, longitude_to=106.71594)
        self.assertEqual(7747, result)

    def test_call_distance_3(self):
        result = cal_distance(latitude_from=10.77667, longitude_from=106.66648,
                              latitude_to=10.75397, longitude_to=None)
        self.assertEqual(-1, result)

    def test_call_distance_4(self):
        result = cal_distance(latitude_from=10.77667, longitude_from=106.66648,
                              latitude_to="10.75397a", longitude_to=106.71594)
        self.assertEqual(-1, result)


if __name__ == '__main__':
    unittest.main()


