import unittest
from testing.mock_data import MockData
from main import create_app
from trip.trip_api import post_trip


class TestTripPost(unittest.TestCase):
    def setUp(self):
        self.app = create_app(config='TEST')

    def test_post_trip_1(self):
        result = post_trip(data=None)
        self.assertEqual({"response": "NO DATA"}, result)

    def test_post_trip_2(self):
        data = MockData.mock_data_trip_create()
        result = post_trip(data=data)
        self.assertEqual({"response": "SUCCESS"}, result)

    def test_post_trip_3(self):
        data = MockData.mock_data_trip_duplicate()
        result = post_trip(data=data)
        self.assertEqual({"response": "DUPLICATE"}, result)


if __name__ == '__main__':
    unittest.main()
