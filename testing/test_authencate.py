import unittest
from main import create_app
from trip.trip_api import authenticate


class TestAuthenticate(unittest.TestCase):
    def setUp(self):
        self.app = create_app(config='TEST')

    def test_authenticate_1(self):
        result = authenticate('')
        self.assertEqual(False, result)

    def test_authenticate_2(self):
        result = authenticate('aasd')
        self.assertEqual(False, result)

    def test_authenticate_3(self):
        result = authenticate(23156)
        self.assertEqual(False, result)

    def test_authenticate_4(self):
        result = authenticate('core')
        self.assertEqual(True, result)

    def test_authenticate_5(self):
        result = authenticate('#$%%^&')
        self.assertEqual(False, result)


if __name__ == '__main__':
    unittest.main()
