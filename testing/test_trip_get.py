import unittest
from testing.mock_data import MockData
from main import create_app
from trip.trip_api import get_trip


class TestTripGet(unittest.TestCase):
    def setUp(self):
        self.app = create_app(config='TEST')
        self.client = self.app.test_client()

    def test_get_trip_1(self):
        result = self.client.get('/trip/0')
        self.assertEqual({'response': "NO DATA HAVE TRIP ID 0"}, result.json)

    def test_get_trip_2(self):
        result = self.client.get('/trip')
        self.assertTrue(len(result.data) > 1)

    def test_get_trip_3(self):
        result = self.client.get('/trip/5dea6d7bd66e08f60ff1df6d')
        self.assertIsNotNone(result.json['response'])


if __name__ == '__main__':
    unittest.main()
