import json


class MockData:
    @staticmethod
    def mock_data_null():
        data = {}
        return json.dumps(data)

    @staticmethod
    def mock_data_normal():
        data = {
            "passenger_response": "Accepted",
            "service_key": 1,
            "service_trip_id": 1,
            "start_lat": 10.758271,
            "start_lon": 106.69548,
            "end_lat": 10.7517439,
            "end_lon": 106.686455,
            "start_address": "Phường 5, District 4, Ho Chi Minh City, Vietnam",
            "end_address": "Phường 2, District 8, Ho Chi Minh City, Vietnam",
            "passenger_id": 1,
            "driver_response": "Waiting",
            "distance": 2.5,
            "price": 152000,
            "state": "Pending",
            "promotion_id": 1,
            "promotion_type": "Reward"
        }
        return json.dumps(data)

    @staticmethod
    def mock_data_get_trip():
        data = {'date_create': '2019-12-06 15:02:19.066432',
                'distance': 3164.041343624245,
                'driver_response': 'Waiting',
                'end_address': '135 Nam Kỳ Khởi Nghĩa, Phường Bến Thành, Quận 1, Hồ Chí Minh '
                               '700000, Việt Nam',
                'end_lat': 10.77713,
                'end_lon': 106.695442,
                'passenger_id': 1,
                'passenger_response': 'Accepted',
                'price': 21000,
                'promotion_id': 1,
                'promotion_type': 'Reward',
                'service_key': 1,
                'service_trip_id': '5dea6d7bd66e08f60ff1df6d',
                'start_address': '155 Sư Vạn Hạnh, Phường 12, Quận 10, Hồ Chí Minh, Việt Nam',
                'start_lat': 10.77667,
                'start_lon': 106.66648,
                'state': 'Pending'}
        return json.dumps(data)

    @staticmethod
    def mock_data_update():
        data = {
            "passenger_response": "Accepted",
            "service_key": 1,
            "service_trip_id": "5dea6d7bd66e08f60ff1df6d",
            "start_lat": 10.77667,
            "start_lon": 106.66648,
            "end_lat": 10.77713,
            "end_lon": 106.695442,
            "start_address": "155 Sư Vạn Hạnh, Phường 12, Quận 10, Hồ Chí Minh, Việt Nam",
            "end_address": "135 Nam Kỳ Khởi Nghĩa, Phường Bến Thành, Quận 1, Hồ Chí Minh.",
            "passenger_id": 1,
            "driver_response": "Waiting",
            "distance": 3164.041343624245,
            "price": 152000,
            "state": "Pending",
            "date_create": "2019-12-06 15:02:19.066432",
            "promotion_id": 1,
            "promotion_type": "Reward"
        }
        return json.dumps(data)

    @staticmethod
    def mock_data_miss_attribute():
        data = {
            "service_trip_id": 1,
            "passenger_id": 1,
            "start_lat": 10.758271,
            "start_lon": 106.69548,
            "end_lat": 10.7517439,
            "end_lon": 106.686455,
            "start_address": "Phường 5, District 4, Ho Chi Minh City, Vietnam",
            "end_address": " Phường 2, District 8, Ho Chi Minh City, Vietnam",
            "distance": 2.5,
            "price": 152000,
            "promotion_id": 1
        }
        return json.dumps(data)

    @staticmethod
    def mock_data_duplicate():
        data = {
            "passenger_response": "Accepted",
            "service_key": 1,
            "service_trip_id": 1,
            "start_lat": 10.758271,
            "start_lon": 106.69548,
            "end_lat": 10.7517439,
            "end_lon": 106.686455,
            "start_address": "Phường 5, District 4, Ho Chi Minh City, Vietnam",
            "end_address": "Phường 2, District 8, Ho Chi Minh City, Vietnam",
            "passenger_id": 1,
            "driver_response": "Waiting",
            "distance": 2.5,
            "price": 152000,
            "state": "Pending",
            "promotion_id": 1
        }
        return json.dumps(data)

    @staticmethod
    def mock_data_reject():
        data = {
            "passenger_response": "Rejected",
            "service_key": 1,
            "service_trip_id": 1,
            "start_lat": 10.758271,
            "start_lon": 106.69548,
            "end_lat": 10.7517439,
            "end_lon": 106.686455,
            "start_address": "Phường 5, District 4, Ho Chi Minh City, Vietnam",
            "end_address": "Phường 2, District 8, Ho Chi Minh City, Vietnam",
            "passenger_id": 1,
            "driver_response": "Waiting",
            "distance": 2.5,
            "price": 152000,
            "state": "Pending",
            "promotion_id": 1
        }
        return json.dumps(data)

    @staticmethod
    def mock_data_not_exist():
        data = {
            "passenger_response": "Rejected",
            "service_key": 1,
            "service_trip_id": 0,
            "start_lat": 10.758271,
            "start_lon": 106.69548,
            "end_lat": 10.7517439,
            "end_lon": 106.686455,
            "start_address": "Phường 5, District 4, Ho Chi Minh City, Vietnam",
            "end_address": "Phường 2, District 8, Ho Chi Minh City, Vietnam",
            "passenger_id": 1,
            "driver_response": "Waiting",
            "distance": 2.5,
            "price": 152000,
            "state": "Pending",
            "promotion_id": 1
        }
        return json.dumps(data)

    @staticmethod
    def mock_data_show_trip_detail():
        data = {
            "service_key": 1,
            "start_lat": 10.77667,
            "start_lon": 106.66648,
            "end_lat": 10.75397,
            "end_lon": 106.71594,
            "start_address": "155 Sư Vạn Hạnh, Phường 12, Quận 10, Hồ Chí Minh, Việt Nam",
            "end_address": "144 Nguyễn Thần Hiến Phường 18 Quận 4",
        }
        return json.dumps(data)

    @staticmethod
    def mock_data_show_trip_miss_attribute():
        data = {
            "start_lat": 10.77667,
            "start_lon": 106.66648,
            "end_lat": 10.75397,
            "end_lon": 106.71594,
            "start_address": "155 Sư Vạn Hạnh, Phường 12, Quận 10, Hồ Chí Minh, Việt Nam",
            "end_address": "144 Nguyễn Thần Hiến Phường 18 Quận 4",
        }
        return json.dumps(data)

    @staticmethod
    def mock_data_show_trip_detail_result():
        data = {
            'distance': 5963.441254658046,
            'end_address': '144 Nguyễn Thần Hiến Phường 18 Quận 4',
            'end_lat': 10.75397,
            'end_lon': 106.71594,
            'start_address': '155 Sư Vạn Hạnh, Phường 12, Quận 10, Hồ Chí Minh, Việt Nam',
            'start_lat': 10.77667,
            'start_lon': 106.66648
        }
        return json.dumps(data)

    @staticmethod
    def mock_data_show_trip_negative_lat_lng():
        data = {
            "service_key": 1,
            "start_lat": -10.77667,
            "start_lon": 106.66648,
            "end_lat": 10.75397,
            "end_lon": 106.71594,
            "start_address": "155 Sư Vạn Hạnh, Phường 12, Quận 10, Hồ Chí Minh, Việt Nam",
            "end_address": "144 Nguyễn Thần Hiến Phường 18 Quận 4",
        }
        return json.dumps(data)

    @staticmethod
    def mock_data_show_trip_negative_lat_long_result():
        data = {
            'distance': 2394104.1778242905,
            'end_address': '144 Nguyễn Thần Hiến Phường 18 Quận 4',
            'end_lat': 10.75397,
            'end_lon': 106.71594,
            'start_address': '155 Sư Vạn Hạnh, Phường 12, Quận 10, Hồ Chí '
                             'Minh, Việt Nam',
            'start_lat': -10.77667,
            'start_lon': 106.66648
        }
        return data

    @staticmethod
    def mock_data_trip_create():
        data = {
            "passenger_response": "Accepted",
            "service_key": 1,
            "service_trip_id": 101,
            "start_lat": 10.758271,
            "start_lon": 106.69548,
            "end_lat": 10.7517439,
            "end_lon": 106.686455,
            "start_address": "Phường 5, District 4, Ho Chi Minh City, Vietnam",
            "end_address": "Phường 2, District 8, Ho Chi Minh City, Vietnam",
            "passenger_id": 1,
            "driver_response": "Waiting",
            "distance": 2.5,
            "price": 152000,
            "state": "Pending",
            "promotion_id": 1
        }
        return json.dumps(data)

    @staticmethod
    def mock_data_trip_duplicate():
        data = {
            "passenger_response": "Accepted",
            "service_key": 1,
            "service_trip_id": 101,
            "start_lat": 10.758271,
            "start_lon": 106.69548,
            "end_lat": 10.7517439,
            "end_lon": 106.686455,
            "start_address": "Phường 5, District 4, Ho Chi Minh City, Vietnam",
            "end_address": "Phường 2, District 8, Ho Chi Minh City, Vietnam",
            "passenger_id": 1,
            "driver_response": "Waiting",
            "distance": 2.5,
            "price": 152000,
            "state": "Pending",
            "promotion_id": 1
        }
        return json.dumps(data)

    @staticmethod
    def mock_trip_query_null():
        data = {}
        return json.dumps(data)

    @staticmethod
    def mock_trip_query_normal():
        data = {
            "id_driver": 924,
            "date_create": "2019"
        }
        return json.dumps(data)

    @staticmethod
    def mock_trip_query_missing_attribute():
        data = {
            "id_driver": 924
        }
        return json.dumps(data)

