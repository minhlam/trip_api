import unittest
from testing.mock_data import MockData
from main import create_app
import json

class Testdriver(unittest.TestCase):
    def setUp(self):
        self.app = create_app(config='TEST')
        self.client = self.app.test_client()

    def test_Accepted_UTCID01(self):
        data = {
            'driver_response': "",
            'service_trip_id': ""
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
        }
        result = self.client.post('/driver_accepted', data=json.dumps(data))
        self.assertEqual({"response": 'NO DATA'}, result.json)

    def test_Accepted_UTCID02(self):
        data = {
            'driver_response': "",
            'service_trip_id': "BIKE1"
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
        }
        result = self.client.post('/driver_accepted', data=json.dumps(data), headers=headers)
        self.assertEqual({"response": 'MISSING DRIVER_RESPONSE'}, result.json)

    def test_Accepted_UTCID03(self):
        data = {
            'driver_response': "",
            'service_trip_id': "ABC",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_accepted', data=json.dumps(data), headers=headers)
        self.assertEqual({"response": 'MISSING DRIVER_RESPONSE'}, result.json)

    def test_Accepted_UTCID04(self):
        data = {
            'driver_response': "Accepted",
            'service_trip_id': "",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_accepted', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'MISSING SERVICE_TRIP_ID'}, result.json)

    def test_Accepted_UTCID05(self):
        data = {
            'driver_response': "Accepted",
            'service_trip_id': "BIKE1",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_accepted', data=json.dumps(data), headers=headers)
        self.assertEqual({"response": 'DRIVER ACCEPTED'}, result.json)

    def test_Accepted_UTCID06(self):
        data = {
            'driver_response': "Accepted",
            'service_trip_id': "ABC",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_accepted', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'NO TRIP ID IS ABC'}, result.json)

    def test_Accepted_UTCID07(self):
        data = {
            'driver_response': "Rejected",
            'service_trip_id': "",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_accepted', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'MISSING SERVICE_TRIP_ID'}, result.json)

    def test_Accepted_UTCID08(self):
        data = {
            'driver_response': "Rejected",
            'service_trip_id': "BIKE1",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_accepted', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'ERROR'}, result.json)

    def test_Accepted_UTCID09(self):
        data = {
            'driver_response': "Rejected",
            'service_trip_id': "ABC"
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
        }
        result = self.client.post('/driver_accepted', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'NO TRIP ID IS ABC'}, result.json)

    def test_Accepted_UTCID10(self):
        data = {
            'driver_response': "Waitting",
            'service_trip_id': ""
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_accepted', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'MISSING SERVICE_TRIP_ID'}, result.json)

    def test_Accepted_UTCID11(self):
        data = {
            'driver_response': "Waitting",
            'service_trip_id': "BIKE1"
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_accepted', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'ERROR'}, result.json)

    def test_Accepted_UTCID12(self):
        data = {
            'driver_response': "Waitting",
            'service_trip_id': "ABC",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_accepted', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'NO TRIP ID IS ABC'}, result.json)

    def test_Accepted_UTCID13(self):
        data = {
            'driver_response': "ABC",
            'service_trip_id': "",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_accepted', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'MISSING SERVICE_TRIP_ID'}, result.json)

    def test_Accepted_UTCID14(self):
        data = {
            'driver_response': "ABC",
            'service_trip_id': "BIKE1",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_accepted', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'ERROR'}, result.json)

    def test_Accepted_UTCID15(self):
        data = {
            'driver_response': "ABC",
            'service_trip_id': "ABC",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_accepted', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'NO TRIP ID IS ABC'}, result.json)

    def test_Rejected_UTCID01(self):
        data = {
            'driver_response': "",
            'service_trip_id': "",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_rejected', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'NO DATA'}, result.json)

    def test_Rejected_UTCID02(self):
        data = {
            'driver_response': "",
            'service_trip_id': "BIKE1",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_rejected', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'MISSING DRIVER_RESPONSE'}, result.json)

    def test_Rejected_UTCID03(self):
        data = {
            'driver_response': "",
            'service_trip_id': "ABC",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_rejected', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'MISSING DRIVER_RESPONSE'}, result.json)

    def test_Rejected_UTCID04(self):
        data = {
            'driver_response': "Rejected",
            'service_trip_id': "",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_rejected', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'MISSING SERVICE_TRIP_ID'}, result.json)

    def test_Rejected_UTCID05(self):
        data = {
            'driver_response': "Rejected",
            'service_trip_id': "BIKE1",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_rejected', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'DRIVER REJECTED'}, result.json)

    def test_Rejected_UTCID06(self):
        data = {
            'driver_response': "Rejected",
            'service_trip_id': "ABC",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_rejected', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'NO TRIP ID IS ABC'}, result.json)

    def test_Rejected_UTCID07(self):
        data = {
            'driver_response': "Accepted",
            'service_trip_id': "",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_rejected', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'MISSING SERVICE_TRIP_ID'}, result.json)

    def test_Rejected_UTCID08(self):
        data = {
            'driver_response': "Accepted",
            'service_trip_id': "BIKE1",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_rejected', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'ERROR'}, result.json)

    def test_Rejected_UTCID09(self):
        data = {
            'driver_response': "Accepted",
            'service_trip_id': "ABC",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_rejected', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'NO TRIP ID IS ABC'}, result.json)

    def test_Rejected_UTCID10(self):
        data = {
            'driver_response': "Waitting",
            'service_trip_id': "",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_rejected', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'MISSING SERVICE_TRIP_ID'}, result.json)

    def test_Rejected_UTCID11(self):
        data = {
            'driver_response': "Waitting",
            'service_trip_id': "BIKE1",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_rejected', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'ERROR'}, result.json)

    def test_Rejected_UTCID12(self):
        data = {
            'driver_response': "Waitting",
            'service_trip_id': "ABC",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_rejected', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'NO TRIP ID IS ABC'}, result.json)

    def test_Rejected_UTCID13(self):
        data = {
            'driver_response': "ABC",
            'service_trip_id': "",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_rejected', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'MISSING SERVICE_TRIP_ID'}, result.json)

    def test_Rejected_UTCID14(self):
        data = {
            'driver_response': "ABC",
            'service_trip_id': "BIKE1",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_rejected', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'ERROR'}, result.json)

    def test_Rejected_UTCID15(self):
        data = {
            'driver_response': "ABC",
            'service_trip_id': "ABC",
        }
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver_rejected', data=json.dumps(data), headers=headers)
        self.assertEqual({"response":'NO TRIP ID IS ABC'}, result.json)




if __name__ == '__main__':
    unittest.main()