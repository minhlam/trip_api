import unittest
from main import create_app
from testing.mock_data import MockData


class TestTripQuery(unittest.TestCase):
    def setUp(self):
        self.app = create_app(config='TEST')
        self.client = self.app.test_client()

    def test_trip_query_1(self):
        data = MockData.mock_trip_query_null()
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver/core', data=data, headers=headers)
        self.assertEqual(b'THIS REQUEST IS INVALID', result.data)

    def test_trip_query_2(self):
        data = MockData.mock_trip_query_normal()
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver/core', data=data, headers=headers)
        self.assertIsNotNone(result.json)

    def test_trip_query_3(self):
        data = MockData.mock_trip_query_normal()
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver/casdsaf', data=data, headers=headers)
        self.assertEqual(b'YOU ARE NOT ALLOWED', result.data)

    def test_trip_query_4(self):
        data = MockData.mock_trip_query_missing_attribute()
        mimetype = 'application/json'
        headers = {
            'Content-Type': mimetype,
            'Accept': mimetype
        }
        result = self.client.post('/driver/core', data=data, headers=headers)
        self.assertEqual(b'THIS REQUEST IS INVALID', result.data)


if __name__ == '__main__':
    unittest.main()
