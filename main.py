from flask_script import Manager
from flask import Flask
from trip.trip_api import blue
from flask_cors import CORS


def create_app(config='dev'):
    app = Flask(__name__)
    app.config['SECRET_KEY'] = '123456789'
    app.register_blueprint(blue)
    CORS(app)
    return app

# app = Flask(__name__)
# app.config['SECRET_KEY'] = '123456789'
# app.register_blueprint(blue)
# CORS(app)

# manager = Manager(create_app())
# manager.add_option('-c', '--config', dest='config', required=False)

app = create_app()

if __name__ == '__main__':
    app.run()
    # app.run(host='192.168.1.126', port=5050)

