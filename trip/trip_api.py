import math
from database_connection.connection import ConnectionString
from flask import Blueprint, request, jsonify
from flask_cors import cross_origin
import datetime
import re
import googlemaps
import json
import requests

db_trip = ConnectionString.init_db('trip')
db_driver = ConnectionString.init_db('driver')
blue = Blueprint('admin', __name__)
gmaps = googlemaps.Client(key='AIzaSyBp1zbhrcngsbN8eIBJsrxBH2FGrsyHNjs')


def check_valid_number(number):
    result = False
    convert_string = str(number)
    if re.match(r"[+]?\d*\.\d+|\d+", convert_string):
        result = True
    return result

def cal_distance(latitude_from, longitude_from, latitude_to, longitude_to):
    list_params = [latitude_from, longitude_from, latitude_to, longitude_to]
    for i in list_params:
        if check_valid_number(i) is False:
            # if one of these params is not number it will return -1
            return -1
    my_response = gmaps.distance_matrix((latitude_from, longitude_from),
                                        (latitude_to, longitude_to))['rows'][0]['elements'][0]
    get_value = my_response['distance']
    return get_value['value']

@blue.route('/trip/<service_trip_id>', methods=['GET'])
@cross_origin()
def get_trip(service_trip_id):
    trips = db_trip.find_one({"service_trip_id": service_trip_id}, {"_id": 0})
    if trips:
        return {'response': trips}
    else:
        return {'response': "NO DATA HAVE TRIP ID %s" % service_trip_id}

@blue.route('/trip', methods=['GET'])
@cross_origin()
def get_all_trip():
    trips = db_trip.find({}, {"_id": 0})
    list_trip = []
    for trip in trips:
        list_trip.append(trip)
    return {'response': list_trip}

@blue.route('/trip', methods=['POST'])
@cross_origin()
def post_trip(data):
    if not data:
        return {"response": "NO DATA"}
    else:
        result = db_trip.find_one({"service_trip_id": data.get('service_trip_id')})
        if result:
            return {"response": "DUPLICATE"}
        else:
            db_trip.insert_one(data)
            return {"response": "SUCCESS"}

# nghia
def authenticate(id):
    if id:
        result = False
        identify = ['core', 'food', 'move']
        for i in identify:
            if i == id:
                result = True
                break
        return result
    else:
        return False

# nghia
@blue.route('/driver/<id_identify>', methods=['POST'])
@cross_origin()
def trip_query(id_identify):
    check_id = authenticate(id_identify)
    data = request.get_json()
    if check_id and len(data) == 2:
        try:
            id_driver = data.get('id_driver')
            date = str(data.get('date_create'))
            get_data = db_trip.find({'id_driver': id_driver, 'date_create': {'$regex': date}}, {'_id': 0})
            result = []
            for i in get_data:
                result.append(i)
            return {'response': result}
        except Exception:
            return 'DATA IS INVALID'
    elif check_id and len(data) < 2:
        return 'THIS REQUEST IS INVALID'
    else:
        return 'YOU ARE NOT ALLOWED'

@blue.route('/report/<id_identify>', methods=['POST'])
@cross_origin()
def trip_report(id_identify):
    check_id = authenticate(id_identify)
    data = request.get_json()
    if check_id and data:
        try:
            date = str(data.get('date_create'))
            # count trip by month
            count_trip = db_trip.find({'date_create': {'$regex': date}}).count()

            # count trip cancel by month
            count_trip_cancel = db_trip.find({'date_create': {'$regex': date}, 'state': {'$regex': 'Cancel'}}).count()

            # count trip complete by month
            count_trip_complete = db_trip.find({'date_create': {'$regex': date}, 'state': {'$regex': 'Complete'}}).count()
            result = {'total_trip': count_trip, 'total_cancel': count_trip_cancel, 'total_complete': count_trip_complete}
            return result
        except Exception:
            return 'DATA IS INVALID'

    elif check_id is False:
        return 'YOU ARE NOT ALLOWED'
    else:
        return 'THIS REQUEST IS INVALID'

def update_trip(service_trip_id, data):
    if not data:
        return {"response": "NO DATA"}

    if not service_trip_id:
        return {"response": "MISSING SERVICE_TRIP_ID"}
    query = {'service_trip_id': service_trip_id}
    value = {"$set": data}
    db_trip.update_one(query, value)
    return {"response": "SUCCESS"}

@blue.route('/show_trip', methods=['POST'])
@cross_origin()
def post_show_trip_detail():
    data = request.get_json()
    if not data:
        return {"response": "NO DATA RECEIVE"}
    else:
        service_key = data.get('service_key')
        start_lat = data.get('start_lat')
        start_lon = data.get('start_lon')
        start_address = data.get('start_address')
        end_lat = data.get('end_lat')
        end_lon = data.get('end_lon')
        end_address = data.get('end_address')

        if not service_key or not start_lat or not start_lon or not start_address or not end_lat or not end_lon or not end_address:
            return {"response": "MISSING DATA"}
        else:
            distance = cal_distance(float(start_lat), float(start_lon), float(end_lat), float(end_lon))
            data_raw = {
                'start_lat': start_lat,
                'start_lon': start_lon,
                'end_lat': end_lat,
                'end_lon': end_lon,
                'start_address': start_address,
                'end_address': end_address,
                'distance': distance,
            }
            return {"response": data_raw}

def check_service(service_key):
    if service_key == 1:
        return "BIKE"    
    elif service_key == 2:
        return "FOOD"
    elif service_key == 3:
        return "MOVE"
    elif service_key == 4:
        return "SHOPPING" 

def check_driver(start_lat, start_lon, list_driver):
    lat1 = start_lat + 0.5
    lat2 = start_lat - 0.5
    lon1 = start_lon + 0.5
    lon2 = start_lon - 0.5
    result = list(filter(lambda x: (x['lat'] < lat1 and x['lat'] > lat2) and (x['lon'] < lon1 and x['lon'] > lon2),list_driver))
    return result

def check_wallet_passenger(passenger_id, payment_type, price):
    j = {
        'user_id': passenger_id,
        'method': payment_type,
        'value': price,
    }
    r = requests.post('https://waza-payment.herokuapp.com/check_passenger_wallet', json=j)
    text = json.loads(r.text)
    response = text.get('response')
    return response

def get_list_driver(service_key):
    if service_key == 1:
        r = requests.get('http://waza-core.ddns.net/api/drivers/listalready/BIKE')
    elif service_key == 2:
        r = requests.get('http://waza-core.ddns.net/api/drivers/listalready/FOOD')
    elif service_key == 3:
        r = requests.get('http://waza-core.ddns.net/api/drivers/listalready/MOVE')
    elif service_key == 4:
        r = requests.get('http://waza-core.ddns.net/api/drivers/listalready/SHOPPING')
    text = json.loads(r.text)
    list_driver = []
    if text.get('data') != None:        
        list_driver = text.get('data')     
    return list_driver

def get_payment_driver(list_payment, price, payment_service):
    j = {
        'list_driver': list_payment,
        'value': price,
        'service': payment_service,                
    }
    r = requests.post('http://waza-payment.herokuapp.com/check_list_driver', json=j)
    text = json.loads(r.text)
    list_payment = text.get('response')   
    return list_payment

def get_top5(list_payment):
    j = {
        'listDriver': list_payment
    }
    r = requests.post('https://safe-caverns-44957.herokuapp.com/API/Driver/GetTop5', json=j)
    text = json.loads(r.text)   
    return text

def push_to_bike_driver(service_trip_id, list_ranking, payment_service, start_lat, start_lon, end_lat, end_lon, start_address, end_address, passenger_id, price, payment_type, distance):
    j = {
        'service_trip_id': service_trip_id,
        'list_driver': list_ranking,
        'service': payment_service,
        'start_lat': start_lat,
        'start_lon': start_lon,
        'end_lat': end_lat,
        'end_lon': end_lon,
        'start_address': start_address,
        'end_address': end_address,
        'passenger_id': passenger_id,
        'price': price,
        'payment_type': payment_type,
        'distance': distance,
    }
    r = requests.post('http://waza-core.ddns.net/api/drivers/trip', json=j)
    text = json.loads(r.text)
    response = text.get('status')
    return response

def push_to_shopping_driver(service_trip_id, list_ranking, payment_service, start_lat, start_lon, end_lat, end_lon, start_address, end_address, passenger_id, price, payment_type, distance):
    j = {
        'service_trip_id': service_trip_id,
        'list_driver': list_ranking,
        'service': payment_service,
        'start_lat': start_lat,
        'start_lon': start_lon,
        'end_lat': end_lat,
        'end_lon': end_lon,
        'start_address': start_address,
        'end_address': end_address,
        'passenger_id': passenger_id,
        'price': price,
        'payment_type': payment_type,
        'distance': distance,
    }
    headers = {
        'Content-Type': 'application/json',
        'X-Parse-Application-Id': 'hHdDuClDuzXuRxnYmWOzNZ4qxZW7MVHq61u',
        'X-Parse-REST-API-Key': 'B9ncAZ4bTyY8RYyKvNQOzwrRTo27ds0BINr',
    }
    r = requests.post('http://waza-shopping-server.herokuapp.com/api/functions/listDriverForWait', headers=headers, json=j)
    print(r)
    text = json.loads(r.text)
    print(text)
    response = text.get('result')
    return response

def check_state_trip(trip_found):
    if trip_found.get('state') == "Pending":
        return 1
    elif trip_found.get('state') == "OnWayToPass":
        return 2
    elif trip_found.get('state') == "OnWayToDes":
        return 3
    elif trip_found.get('state') == "Complete":
        return 4
    elif trip_found.get('state') == "Cancel":
        return -1

def change_passenger_state(serviceName, state, passenger_id):    
    j = {
        'serviceName': serviceName,
        'state': state,
        'id': passenger_id,
    }
    r = requests.post('http://waza-core.ddns.net/api/passengers/updatestate', json=j)
    text = json.loads(r.text)
    print(text)
    response = text.get('status')
    return response

def change_driver_state(serviceName, state, driver_id):
    j = {
        'serviceName': serviceName,
        'state': state,
        'id': driver_id,
    }
    r = requests.post('http://waza-core.ddns.net/api/drivers/updatestate', json=j)
    text = json.loads(r.text)
    print(text)
    response = text.get('status')
    return response

@blue.route('/pass_accepted', methods=['POST'])
@cross_origin()
def passenger_accepted():
    data = request.get_json()
    if not data:
        return {"response": "NO DATA RECEIVE"}

    passenger_response = data.get('passenger_response')
    service_trip_id = data.get('service_trip_id')
    service_key = data.get('service_key')
    start_lat = data.get('start_lat')
    start_lon = data.get('start_lon')
    start_address = data.get('start_address')
    end_lat = data.get('end_lat')
    end_lon = data.get('end_lon')
    end_address = data.get('end_address')
    passenger_id = data.get('passenger_id')
    distance = data.get('distance')
    price = data.get('price')
    original_price = data.get('original_price')
    promotion_id = data.get('promotion_id')
    promotion_type = data.get('promotion_type')
    payment_type = data.get('payment_type')    

    if promotion_id == None or promotion_type == None:
        return {"response": "MISSING PROMOTION ID OR PROMOTION TYPE", "status": False}
    
    if not original_price or not distance or not price or not passenger_response or not service_trip_id or not service_key or not start_lat or not start_lon or not start_address or not end_lat or not end_lon or not end_address or not passenger_id or not payment_type:
        return {"response": "MISSING DATA", "status": False}
    
    if isinstance(promotion_id, str) == False or isinstance(promotion_type, str) == False or isinstance(payment_type, str) == False:
        return {"response": "WRONG PROMOTION_ID TYPE OR PROMOTION_TYPE TYPE OR PAYMENT_TYPE", "status": False}   

    if isinstance(price, str) == True or isinstance(original_price, str) == True or isinstance(distance, str) == True:
        return {"response": "WRONG PRICE TYPE OR ORIGINAL_PRICE TYPE OR DISTANCE TYPE", "status": False}

    trip_found = db_trip.find_one({"service_trip_id": service_trip_id})
    if trip_found:
        return {"response": "DUPLICATE", "status": False}
    else:
        if passenger_response == "Accepted":
            trip_raw = {
                'passenger_response': passenger_response,
                'service_key': service_key,
                'service_trip_id': service_trip_id,
                'start_lat': start_lat,
                'start_lon': start_lon,
                'end_lat': end_lat,
                'end_lon': end_lon,
                'start_address': start_address,
                'end_address': end_address,
                'passenger_id': passenger_id,
                'driver_response': 'Waiting',
                'id_driver': "Finding", #New
                'driver_name': "Finding", #New
                'license_plate': "Finding", #New
                'date_create': str(datetime.datetime.utcnow()),
                'distance': distance,
                'price': price,
                'original_price': original_price,
                'state': "Pending",
                'promotion_id': promotion_id,
                'promotion_type': promotion_type,
                'payment_type': payment_type, #New
            }
            print(trip_raw)
            # Check passenger wallet
            if check_wallet_passenger(passenger_id, payment_type, price) == False:
                print(["PAYMENT CHECK PASSENGER WALLET: NOT ENOUGH MONEY"])
                return {"response": "NOT ENOUGH MONEY", "status": False}  

            # Create Trip
            post_trip(trip_raw)

            # Get List Driver
            list_driver = [] 
            try:
                list_driver = get_list_driver(service_key)
                for driver in list_driver:
                    if driver.get('lat') == None or driver.get('lon') == None:
                        return {"response": "WHERE IS LAT LON DRIVER??", "status": False}

                if list_driver == []:
                    return {"response": "NO DRIVER AROUND HERE (HOANG LONG)", "status": False}
                print("list_driver")
                print(list_driver)
            except:
                return {"response": "FAIL WHEN GET LIST DRIVER FROM CORE", "status": False}                
                                                                           
            # Get Driver in range
            result = []
            try:
                result = check_driver(start_lat, start_lon, list_driver)
                if result == []:
                    return {"response": "NO DRIVER AROUND HERE (LAM)", "status": False}
                print("result")
                print(result)
            except:
                return {"response": "FAIL WHEN GET LIST DRIVER IN RANGE", "status": False}
                 
            # Get Driver in Payment
            list_payment = []     
            try:
                for item in result:
                    list_payment.append(item.get('_id'))                                                

                payment_service = check_service(service_key)            
                if payment_type == "CASH":
                    list_payment = get_payment_driver(list_payment, price, payment_service)
                if list_payment == []:
                    return {"response": "NO DRIVER AROUND HERE (TRONG TU)", "status": False}  

                print("list_payment")
                print(list_payment) 
            except:
                return {"response": "FAIL WHEN GET LIST DRIVER FROM PAYMENT", "status": False}

            # Get Top 5 Driver
            try:
                list_ranking = []
                text = get_top5(list_payment)                                    
                for driver in text:
                    list_ranking.append(driver.get('UserID'))
                if list_ranking == []:
                    return {"response": "NO DRIVER AROUND HERE (DUY KHOA)", "status": False}  

                print("list_ranking")
                print(list_ranking)
            except:
                return {"response": "FAIL WHEN GET LIST DRIVER FROM RANKING", "status": False}

            # Push to driver     
            try:
                response = 'fail'
                if service_key == 1:
                    response = push_to_bike_driver(service_trip_id, list_ranking, payment_service, start_lat, start_lon, end_lat, end_lon, start_address, end_address, passenger_id, price, payment_type, distance)            
                elif service_key == 4:
                    response = push_to_shopping_driver(service_trip_id, list_ranking, payment_service, start_lat, start_lon, end_lat, end_lon, start_address, end_address, passenger_id, price, payment_type, distance)               
                
                if response == 'success':
                    # Change state
                    serviceName = check_service(service_key)
                    try:                        
                        response = change_passenger_state(serviceName, "waittingdriver", passenger_id)
                        print([serviceName, passenger_id])
                        if response != 'success':
                            return {"response": "CANT CHANGE PASSENGER STATE", "status": False}
                    except:
                        return {"response": "FAIL TO CHANGE PASSENGER STATE", "status": False}
                    return {"response": "Finding Driver", "status": True}
                else:
                    return {"response": "NO DRIVER AROUND HERE (D)", "status": False}
            except:
                return {"response": "FAIL TO PUSH DRIVER TO SERVICE", "status": False}                      
        else:
            return {'response': 'ERROR', "status": False}


@blue.route('/pass_rejected', methods=['POST'])
@cross_origin()
def pass_rejected():
    data = request.get_json()
    if not data:
        return {"response": "NO DATA RECEIVE", "status": False}

    passenger_response = data.get('passenger_response')
    service_trip_id = data.get('service_trip_id')
    passenger_id = data.get('passsenger_id')

    if not service_trip_id:
        return {"response": "MISSING SERVICE TRIP ID", "status": False}

    if not passenger_response:
        return {"response": "MISSING PASSENGER RESPONSE", "status": False}

    if not passenger_id:
        return {"response": "MISSING PASSENGER ID", "status": False}

    trip_found = db_trip.find_one({"service_trip_id": service_trip_id})
    if not trip_found:
        return {"response": "NO TRIP ID IS %s" % service_trip_id, "status": False}

    state = check_state_trip(trip_found)
    if state == 3 or state == -1 or state == 4:
        return {"response": "YOU CANT CANCEL NOW", "status": False}
    
    # Change passenger state     
    serviceName = check_service(trip_found.get('service_key'))
    try:
        response = change_passenger_state(serviceName, "free", passenger_id)
        if response != 'success':
            return {"response": "CANT CHANGE PASSENGER STATE", "status": False}                
    except:
        return {"response": "FAIL TO CHANGE PASSENGER STATE", "status": False}

    if state == 2:
        # Change driver state    
        try:
            response = change_driver_state(serviceName, "alreadyPickUp", trip_found.get('id_driver'))
            if response != 'success':
                return {"response": "CANT CHANGE DRIVER STATE", "status": False}
        except:
            return {"response": "FAIL TO CHANGE DRIVER STATE", "status": False}

    # Call Ranking to level down    
    try:
        j = {
            'passengerID': passenger_id
        }
        r = requests.post('https://safe-caverns-44957.herokuapp.com/API/Driver/CancelByPassenger', json=j)
    except:
        return {"response": "FAIL TO CALL RANKING TO LEVEL DOWN PASSENGER", "status": False}
    
    if passenger_response == "Canceled":
        trip_raw = {
            'passenger_response': passenger_response, 
            'state': 'Cancel'
        }
        update_trip(service_trip_id, trip_raw)           
        
        return {"response": "PASSENGER CANCELED", "status": True}
    return {"response": "ERROR", "status": False}

@blue.route('/driver_accepted', methods=['POST'])
@cross_origin()
def driver_accepted():
    data = request.get_json()
    if not data:
        return {"response": "NO DATA", "status": False}

    list_driver_rejected = []

    driver_response = data.get('driver_response')
    service_trip_id = data.get('service_trip_id')
    list_driver_rejected = data.get('list_driver_rejected')
    id_driver = data.get('id_driver')
    driver_name = data.get('driver_name')
    license_plate = data.get('license_plate')

    if not service_trip_id:
        return {"response": "MISSING SERVICE_TRIP_ID", "status": False}

    if list_driver_rejected == None:
        return {"response": "MISSING LIST_DRIVER_REJECTED", "status": False}  

    if not driver_response:
        return {"response": "MISSING DRIVER_RESPONSE", "status": False}    

    if not id_driver:
        return {"response": "MISSING DRIVER_ID", "status": False}

    trip_found = db_trip.find_one({"service_trip_id": service_trip_id})
    if not trip_found:
        return {"response": "NO TRIP ID IS %s" % service_trip_id, "status": False}

    # Check state
    state = check_state_trip(trip_found)
    if state != 1:
        return {"response": "YOU CANT ACCEPT NOW", "status": False}
        
    if driver_response == "Accepted":
        # Call Ranking
        try:
            j = {
                'driverID': list_driver_rejected
            }
            r = requests.post('https://safe-caverns-44957.herokuapp.com/API/Driver/CancelByDriver', json=j) 
        except:
            return {"response": "FAIL TO CALL RANKING TO LEVEL DOWN LIST DRIVER", "status": False}

        trip_raw = {
            'driver_response': driver_response,
            'state': 'OnWayToPass',
            'id_driver': id_driver, 
            'driver_name': driver_name,
            'license_plate': license_plate,          
        }                               
        update_trip(service_trip_id, trip_raw)

        # Change driver state
        serviceName = check_service(trip_found.get('service_key'))
        try:
            response = change_driver_state(serviceName, "OnWayToPass", id_driver)
            if response != 'success':
                return {"response": "CANT CHANGE DRIVER STATE", "status": False}
        except:
            return {"response": "FAIL TO CHANGE DRIVER STATE", "status": False}
        
        return {"response": "DRIVER ACCEPTED", "status": True}
    return {"response": "ERROR", "status": False}

@blue.route('/driver_rejected', methods=['POST'])
@cross_origin()
def driver_rejected():
    data = request.get_json()
    if not data:
        return {"response": "NO DATA", "status": False}

    list_driver_rejected = data.get('list_driver_rejected')
    service_trip_id = data.get('service_trip_id')

    if not service_trip_id:
        return {"response": "MISSING SERVICE_TRIP_ID", "status": False}

    if not list_driver_rejected:
        return {"response": "MISSING LIST_DRIVER_REJECTED", "status": False}    

    trip_found = db_trip.find_one({"service_trip_id": service_trip_id})
    if not trip_found:
        return {"response": "NO TRIP ID IS %s" % service_trip_id}
    
    # Check State
    state = check_state_trip(trip_found)
    if state != 1:
        return {"response": "YOU CANT REJECT NOW", "status": False}    
    
    # Call Ranking to level down  
    try:
        j = {
            'driverID': list_driver_rejected
        }
        r = requests.post('https://safe-caverns-44957.herokuapp.com/API/Driver/CancelByDriver', json=j)
    except:
        return {"response": "FAIL TO CALL RANKING TO LEVEL DOWN LIST DRIVER", "status": False}     

    trip_raw = {
        'driver_response': "Rejected",
        'state': 'Cancel',
    }
    update_trip(service_trip_id, trip_raw)
    # Drop Trip
    return {"response": "NO DRIVER AROUND HERE", "status": True}
            
            
@blue.route('/driver_canceled', methods=['POST'])
@cross_origin()
def driver_canceled():
    data = request.get_json()
    if not data:    
        return {"response": "NO DATA", "status": False}

    id_driver = data.get('id_driver')
    service_trip_id = data.get('service_trip_id')
    driver_response = data.get('driver_response')

    if not service_trip_id:
        return {"response": "MISSING SERVICE_TRIP_ID", "status": False}

    if not driver_response:
        return {"response": "MISSING DRIVER_RESPONSE", "status": False}

    if not id_driver:
        return {"response": "MISSING DRIVER ID", "status": False}

    trip_found = db_trip.find_one({"service_trip_id": service_trip_id})
    if not trip_found:
        return {"response": "NO TRIP ID IS %s" % service_trip_id, "status": False}

    # Check State
    state = check_state_trip(trip_found)
    if state != 2:
        return {"response": "YOU CANT CANCEL NOW", "status": False}    

    # Driver Rejected after Accepted Trip    
    if driver_response == "Canceled":
        # Change driver state
        serviceName = check_service(trip_found.get('service_key'))
        try:
            response = change_driver_state(serviceName, "alreadyPickUp", id_driver)
            if response != 'success':
                return {"response": "CANT CHANGE DRIVER STATE", "status": False}
        except:
            return {"response": "FAIL TO CHANGE DRIVER STATE", "status": False} 

        # Call Ranking to level down
        try:
            list_driver_rejected = []
            list_driver_rejected.append(id_driver)
            j = {
                'driverID': list_driver_rejected
            }
            r = requests.post('https://safe-caverns-44957.herokuapp.com/API/Driver/CancelByDriver', json=j)        
        except:
            return {"response": "FAIL TO CALL RANKING TO LEVEL DOWN DRIVER", "status": False} 

        # Update Trip
        trip_raw = {
            'driver_response': driver_response,
            'state': 'Cancel',
        }
        update_trip(service_trip_id, trip_raw)                
    else:
        return {"response": "WRONG RESPONSE", "status": False}    
    return {"response": "DRIVER CANCLED", "status": True}                

@blue.route('/passenger_located', methods=['POST'])
@cross_origin()
def passenger_located():
    data = request.get_json()
    if not data:
        return {"response": "NO DATA", "status": False}

    service_trip_id = data.get('service_trip_id')

    if not service_trip_id:
        return {"response": "MISSING SERVICE_TRIP_ID", "status": False}

    trip_found = db_trip.find_one({"service_trip_id": service_trip_id})
    if not trip_found:
        return {"response": "NO TRIP ID IS %s" % service_trip_id, "status": False}

    # Check State
    state = check_state_trip(trip_found)
    if state != 2:
        return {"response": "YOUR TRIP STATE IS NOT ON WAY TO PASSENGER", "status": False}  

    trip_raw = {
        'state': 'OnWayToDes',
    }
    update_trip(service_trip_id, trip_raw)

    # Change driver state
    serviceName = check_service(trip_found.get('service_key'))
    id_driver = trip_found.get('id_driver')
    try:        
        response = change_driver_state(serviceName, "OnWayToDes", id_driver)
        print([serviceName, id_driver, response])
        if response != 'success':
            return {"response": "CANT CHANGE DRIVER STATE", "status": False}
    except:
        return {"response": "FAIL TO CHANGE DRIVER STATE", "status": False}

    return {"response": "ON WAY TO DESTINATION", "status": True}

def call_payment(service, order_id, passenger_id, driver_id, trip_money, discount_money, payment_method, discount_method):
    j = {
        'service': service,
        'order_id': order_id,
        'passenger_id': passenger_id,
        'driver_id': driver_id,
        'trip_money': trip_money,
        'discount_money': discount_money,
        'payment_method': payment_method,
        'discount_method': discount_method,
    }
    r = requests.post('http://waza-payment.herokuapp.com/end_trip', json=j)
    text = json.loads(r.text)    
    print("Call Payment")  
    print(text) 
    response = text.get('response')
    return response

def call_membership(user_id, service, promotion_id, promotion_type):
    service = service.lower()
    promotion_type = promotion_type.lower()
    j = {
        'user_name': user_id,
        'service': service,
        'promotion_id': promotion_id,
        'promotion_type': promotion_type,        
    }
    r = requests.post('http://waza-payment.herokuapp.com/end_trip', json=j)
    text = json.loads(r.text) 
    print("Call Membership")  
    print(text)    
    response = text.get('response')
    return response

def call_reward(user_id, promotion_id):
    j = {
        'uid': user_id,
        'code': promotion_id
    }
    r = requests.post('https://safe-caverns-44957.herokuapp.com/API/Reward/UseReward', json=j)
    text = json.loads(r.text) 
    print("Use Reward")  
    print(text) 
    response = text.get('status')
    return response

def call_ranking(passenger_id, id_driver, price, payment_type, service):
    j = {
        'passengerID': passenger_id,
        'price': price,
        'paymentType': payment_type,
        'serviceID': service,
    }
    r = requests.post('https://safe-caverns-44957.herokuapp.com/API/Point/Passenser', json=j)
    text = json.loads(r.text)    
    passenger_ranking_response = text.get('status')
    print("Passenger Ranking: ")
    print(text)
    j = {
        'driverID': id_driver,
        'price': price, 
    }
    r = requests.post('https://safe-caverns-44957.herokuapp.com/API/Point/Driver', json=j)
    text = json.loads(r.text)   
    print("Driver Ranking: ") 
    print([text, id_driver, price])
    driver_ranking_response = text.get('status')

    if passenger_ranking_response == True and driver_ranking_response == True:
        return True
    return False

def get_service_reward(service):
    service = service.lower()
    r = requests.get('http://safe-caverns-44957.herokuapp.com/API/Service/GetAllService')
    text = json.loads(r.text)
    print("Get Service Trip Reward: ") 
    print(text)
    for item in text:
        if item.get('ServiceName').lower() == service:
            return item.get('_id')
    return "Some Thing Wrong In Get Service Reward"

@blue.route('/destination_located', methods=['POST'])
@cross_origin()
def destination_located():
    data = request.get_json()
    if not data:
        return {"response": "NO DATA"}

    service_trip_id = data.get('service_trip_id')

    if not service_trip_id:
        return {"response": "MISSING SERVICE_TRIP_ID", "status": False}

    trip_found = db_trip.find_one({"service_trip_id": service_trip_id})
    if not trip_found:
        return {"response": "NO TRIP ID IS %s" % service_trip_id, "status": False}
    
    # Check State
    state = check_state_trip(trip_found)
    if state != 3:
        return {"response": "YOUR TRIP STATE IS NOT ON WAY TO DESTINATION", "status": False}

    service = check_service(trip_found.get('service_key'))
    order_id = trip_found.get('service_trip_id')
    passenger_id = trip_found.get('passenger_id')
    driver_id = trip_found.get('id_driver')    
    trip_money = trip_found.get('original_price')
    discount_money = trip_found.get('price')
    payment_method = trip_found.get('payment_type')
    discount_method = trip_found.get('promotion_type')
    promotion_id = trip_found.get('promotion_id')

    # Change driver state    
    try:
        response = change_driver_state(service, "alreadyPickUp", driver_id)
        if response != 'success':
            return {"response": "CANT CHANGE DRIVER STATE", "status": False}
    except:
        return {"response": "FAIL TO CHANGE DRIVER STATE", "status": False}

    # Change passenger state    
    try:
        response = change_passenger_state(service, "free", passenger_id)
        if response != 'success':
            return {"response": "CANT CHANGE PASSENGER STATE", "status": False}
    except:
        return {"response": "FAIL TO CHANGE PASSENGER STATE", "status": False}      

    #Call payment
    try:
        response = call_payment(service, order_id, passenger_id, driver_id, trip_money, discount_money, payment_method, discount_method)
        if response != "SUCCESS":
            return {"response": "NOT SUCCESS AT PAYMENT", "status": False}
    except:
        return {"response": "FAIL TO CALL PAYMENT", "status": False}

    discount_method = discount_method.lower()
    if discount_method == "combo" or discount_method == "voucher":
        # Call membership
        try:
            response = call_membership(passenger_id, service, promotion_id, discount_method)
            if response != "using voucher successfully!":
                return {"response": "NOT SUCCESS AT MEMBERSHIP", "status": False, "membership_response": response}
        except:
            return {"response": "FAIL TO CALL MEMBERSHIP", "status": False}
    elif discount_method == "reward":
        # Call Reward
        try:
            response = call_reward(passenger_id, promotion_id)
            if response != True:
                return {"response": "NOT SUCCESS AT REWARD DISCOUNT", "status": False, "reward_response": response}
        except:
            return {"response": "FAIL TO CALL REWARD DISCOUNT", "status": False}

    # Call Ranking reward   
    try:
        service_reward = get_service_reward(service)
    except:
        return {"response": "FAIL TO GET SERVICE REWARD", "status": False}

    try:
        if payment_method == "CASH":
            payment_method = 1
        elif payment_method == "WALLET":
            payment_method = 2
        response = call_ranking(passenger_id, driver_id, discount_money, payment_method, service_reward)
        if response != True:
            return {"response": "NOT SUCCESS AT REWARD", "status": False}
    except:
        return {"response": "FAIL TO CALL RANKING REWARD", "status": False}

    trip_raw = {
        'state': 'Complete',
    }
    update_trip(service_trip_id, trip_raw)    
    return {"response": "TRIP COMPLETED", "status": True}

@blue.route('/deploy_test', methods=['GET'])
@cross_origin()
def deploy_test():    
    return {"response": "2:43 AM"}

# @blue.route('/driver/<service_key>', methods=['GET'])
# def driver_get(service_key):
#     if service_key:
#         data = []
#         drivers = db_driver.find({}, {"_id": 0})
#         for driver in drivers:
#             if driver['service_key'] == int(service_key):
#                 data.append(driver)
#         return jsonify(data)
#     return {'response': 'NO DATA RECEIVE'}


# @blue.route('/driver', methods=['POST'])
# def driver_post():
#     data = request.get_json()
#     if not data:
#         return {"response": "NO DATA"}
#     cnt = 0
#     for item in data:
#         name = item.get('name')
#         id = item.get('id')
#         service_key = item.get('service_key')
#         phone_number = item.get('phone _number')
#         lat = item.get('lat')
#         lon = item.get('lon')
#         state = item.get('state')

#         if not name or not id or not service_key or not phone_number or not lat or not lon or not state:
#             continue

#         driver_found = db_driver.find_one({"id": id})
#         if driver_found:
#             continue

#         if db_driver.find_one({"id": item.get('id')}):
#             continue
#         else:
#             db_driver.insert(item)
#             cnt += 1
#     return {"response": "Add %s driver" % cnt}

