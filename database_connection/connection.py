from pymongo import MongoClient


class ConnectionString(object):
    client = MongoClient("mongodb+srv://nghia:nghia@cluster0-lupss.gcp.mongodb.net/test?retryWrites=true&w=majority")
    db_name = client['test']

    # client = MongoClient("mongodb://localhost:27017/waza_trip")
    # db_name = client['waza_trip']

    @staticmethod
    def init_db(collection):
        ConnectionString.db = ConnectionString.db_name[collection]
        return ConnectionString.db
